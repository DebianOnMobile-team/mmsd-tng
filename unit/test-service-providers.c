/* SPDX-License-Identifier: LGPL-2.1-or-later */
/*
 * Copyright (C) 2019 Red Hat
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gio/gio.h>

#include "service-providers.h"

static void
test_seperate_mms_apn_cb (const char *apn,
                          const char *mmsc,
                          const char *proxy,
                          const char *mmssize,
                          GError     *error,
                          gpointer    user_data)
{
  GMainLoop *loop = user_data;

  g_main_loop_quit (loop);
  g_assert_no_error (error);
  g_assert_cmpstr (apn, ==, "mms");
  g_assert_cmpstr (mmsc, ==, "http://mms.seperate.example.com/");
  g_assert_cmpstr (proxy, ==, "192.0.2.1:8080");
  g_assert_null (mmssize);
}

/*
 * This tests an APN that has seperate internet and MMS. It lacks the
 * "mmsattachmentsize" attribute. It has an MMS proxy.
 */
static void
test_seperate_mms_apn (void)
{
  GMainLoop *loop = g_main_loop_new (NULL, FALSE);

  mmsd_service_providers_find_settings (SOURCE_ROOT
                                        "/unit/test-service-providers.xml",
                                        "133666",
                                        "mms",
                                        test_seperate_mms_apn_cb,
                                        loop);
  g_main_loop_run (loop);
  g_main_loop_unref (loop);
}

/*****************************************************************************/

static void
test_combined_mms_apn_cb (const char *apn,
                          const char *mmsc,
                          const char *proxy,
                          const char *mmssize,
                          GError     *error,
                          gpointer    user_data)
{
  GMainLoop *loop = user_data;

  g_main_loop_quit (loop);
  g_assert_no_error (error);
  g_assert_cmpstr (apn, ==, "access.example.com");
  g_assert_cmpstr (mmsc, ==, "http://mms.combined.example.com/");
  g_assert_null (proxy);
  g_assert_cmpstr (mmssize, ==, "1048576");
  g_assert_cmpint(1048576, ==, atoi(mmssize));
}

/*
 * This tests an APN that has internet and MMS under the same APN. It also has
 * the "mmsattachmentsize" attribute. It does not have an MMS proxy.
 */
static void
test_combined_mms_apn (void)
{
  GMainLoop *loop = g_main_loop_new (NULL, FALSE);

  mmsd_service_providers_find_settings (SOURCE_ROOT
                                        "/unit/test-service-providers.xml",
                                        "345999",
                                        "access.example.com",
                                        test_combined_mms_apn_cb,
                                        loop);
  g_main_loop_run (loop);
  g_main_loop_unref (loop);
}

/*****************************************************************************/

static void
test_missing_mmsc_apn_cb (const char *apn,
                          const char *mmsc,
                          const char *proxy,
                          const char *mmssize,
                          GError     *error,
                          gpointer    user_data)
{
  GMainLoop *loop = user_data;

  g_main_loop_quit (loop);
  g_assert_error (error, 1, 1);
}

/*
 * The MCC/MNC 123123 exists, and the APN "access.example.com" exists in there,
 * but there is no MMSC under it, so this will not work
 */
static void
test_missing_mmsc_apn (void)
{
  GMainLoop *loop = g_main_loop_new (NULL, FALSE);

  mmsd_service_providers_find_settings (SOURCE_ROOT
                                        "/unit/test-service-providers.xml",
                                        "123123",
                                        "access.example.com",
                                        test_missing_mmsc_apn_cb,
                                        loop);
  g_main_loop_run (loop);
  g_main_loop_unref (loop);
}

/*****************************************************************************/

static void
test_missing_mmsc_apn_two_cb (const char *apn,
                              const char *mmsc,
                              const char *proxy,
                              const char *mmssize,
                              GError     *error,
                              gpointer    user_data)
{
  GMainLoop *loop = user_data;

  g_main_loop_quit (loop);
  g_assert_error (error, 1, 1);
}

/*
 * The MCC/MNC 13337 exists, and the APN "gprs.example.com" exists in there,
 * but there is no MMSC under it, so this will not work
 */
static void
test_missing_mmsc_apn_two (void)
{
  GMainLoop *loop = g_main_loop_new (NULL, FALSE);

  mmsd_service_providers_find_settings (SOURCE_ROOT
                                        "/unit/test-service-providers.xml",
                                        "13337",
                                        "gprs.example.com",
                                        test_missing_mmsc_apn_two_cb,
                                        loop);
  g_main_loop_run (loop);
  g_main_loop_unref (loop);
}

/*****************************************************************************/

static void
test_negative_cb (const char *apn,
                  const char *mmsc,
                  const char *proxy,
                  const char *mmssize,
                  GError     *error,
                  gpointer    user_data)
{
  GMainLoop *loop = user_data;

  g_main_loop_quit (loop);
  g_assert_error (error, 1, 1);
}

/*
 * The MCC/MNC 78130 does not exist.
 */
static void
test_negative (void)
{
  GMainLoop *loop = g_main_loop_new (NULL, FALSE);

  mmsd_service_providers_find_settings (SOURCE_ROOT
                                        "/unit/test-service-providers.xml",
                                        "78130",
                                        "gprs.example.com",
                                        test_negative_cb,
                                        loop);
  g_main_loop_run (loop);
  g_main_loop_unref (loop);
}

/*****************************************************************************/

static void
test_nonexistent_cb (const char *apn,
                     const char *mmsc,
                     const char *proxy,
                     const char *mmssize,
                     GError     *error,
                     gpointer    user_data)
{
  GMainLoop *loop = user_data;

  g_assert_error (error, G_IO_ERROR, G_IO_ERROR_AGAIN);
  g_main_loop_quit (loop);
}

/*
 * Test to see what happens when you point to the wrong xml file, it won't
 * exist
 */
static void
test_nonexistent (void)
{
  GMainLoop *loop = g_main_loop_new (NULL, FALSE);

  mmsd_service_providers_find_settings ("nonexistent.xml",
                                        "13337",
                                        "gprs.example.com",
                                        test_nonexistent_cb,
                                        loop);
  g_main_loop_run (loop);
  g_main_loop_unref (loop);
}

/*****************************************************************************/

int
main (int    argc,
      char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/service-providers/seperate_mms_apn", test_seperate_mms_apn);
  g_test_add_func ("/service-providers/combined_mms_apn", test_combined_mms_apn);
  g_test_add_func ("/service-providers/missing_mmsc_apn", test_missing_mmsc_apn);
  g_test_add_func ("/service-providers/missing_mmsc_apn_two", test_missing_mmsc_apn_two);
  g_test_add_func ("/service-providers/negative", test_negative);
  g_test_add_func ("/service-providers/nonexistent", test_nonexistent);

  return g_test_run ();
}
