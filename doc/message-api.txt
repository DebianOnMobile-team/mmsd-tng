Message hierarchy
=================

Service		org.ofono.mms
Interface	org.ofono.mms.Message
Object path	[variable prefix]/{message0,message1,...}

Methods		void MarkRead()

			Change the status of an incoming message to read.

			This is only supported for incoming messages and is
			meant as user interface driven action.

			Possible Errors: [service].Error.InvalidArguments

		void Delete()

			Delete this messages from storage.

			When deleting a message a MessageRemoved signal will
			be triggered.

			Possible Errors: [service].Error.InvalidArguments

Signals		PropertyChanged(string name, variant value)

			This signal indicates a changed value of the given
			property.

			The only expected property change is for the
			message status.

Properties	string Status [readonly]

			The status of the message.  Possible values are
			"received", "read", "sent", "sending_failed", and "draft".

		string Date [readonly]

			The date of the message. Either when it was
			received or sent.

		string Subject [readonly, optional]

			Contains the optional subject of a MMS.

		string Sender [readonly, optional]

			Number of sender.

		gboolean Delivery Report [readonly]

			Whether the message is requesting a delivery report

		string Delivery Status [readonly, optional]

			A CSV of each phone number in the format of
			"+12345550200=none" where "+12345550200" is the phone
			number and "none" is the delivery status. This will only
			be present if "Delivery Report" is true.

			NOTE: If this is a "PropertyChanged" signal, it will be
			in the format of "delivery_update,+12345550200=none"

		string Modem Number [readonly]

			The modem number associated with the MMS service. While
			it will be with every MMS, this	is meant to make it
			easier to seperate out one's own number from the
			recipients in a group MMS.

		array{string} Recipients [readonly]

			Numbers of recipients.

		string Smil [readonly, optional]

			SMIL body in UTF-8 format.

		array{string id, string content-type, string filename,
			uint64 offset, uint64 len} Attachments [readonly]

			List of attachement content types, identifiers, offset
			and length of data in the stored file.
